﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Báscula_serial
{
    public partial class Form1 : Form
    {
        double Weight = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // Apertura de puerto serial del Arduino
                serialPort1.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo abrir el puerto. Detalles:\n" + ex.Message);
                this.Dispose();
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (serialPort1.BytesToRead > 0)
            {
                // TRY-CATCH por si se da un fallo de comunicación
                try
                {
                    string TextoRecibido = serialPort1.ReadLine();
                    if (TextoRecibido.Length > 0)
                    {
                        //  TextoRecibido tendría este formato ==>   "distancia:197\r"

                        string[] TextoSeccionado = TextoRecibido.Split(':', '\r');
                        // TextoSeccionado para el ejemplo de formato quedaría así:
                        //       TextoSeccionado[0] = "distancia"
                        //       TextoSeccionado[1] = "197"
                        //       TextoSeccionado[2] = ""

                        if (TextoSeccionado.Length == 3)
                        {
                            string NombreVariable = TextoSeccionado[0];
                            string ValorVariable = TextoSeccionado[1];

                            if (NombreVariable == "weight")
                            {
                                // Obtención de la distancia medida por el sensor
                                Weight = Convert.ToDouble(ValorVariable);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // Mostramos el peso, que es tipo Double, como un Strin
            // modificiando el formato para que muestre siempre dos decimales
            label1.Text = Weight.ToString("0.00");
        }
    }
}
