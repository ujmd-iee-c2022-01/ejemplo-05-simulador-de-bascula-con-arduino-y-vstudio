/*
  Arduino Scale Simulator
  
  Programa de que simula ser una Báscula Serial, manipulando el valor
  del peso medido con un Módulo de Joystick o un potenciómetro.
  El valor de peso simulado será enviado a la computadora por medio
  de la comunicación Serial-USB.

  Se preparará el formato de envío a la computadora para que 
  una aplicación de .NET Framework pueda interpretarlo enviando
  el dato con el siguiente formato:
         "variable:valor"
         Ej: "weight:20"
         (unidades en Kg predefinido por nosotros los integradores)

  Para este ejemplo el rango de peso de la báscula será de 0 a 500 Kg.
  
  Este programa está disponible en el siguiente repositorio de GitLab:
  https://gitlab.com/ujmd-iee-c2022-01/.git
*/

// Contantes de los pines conectados al Joystick Module
const int pinJoystick_X = A1;
const int pinJoystick_Y = A0;
const int pinJoystick_SW = 9;

// Constantes globales
  double weight = 0;

void setup() {
  // El switch del Joystick es necesario establecerlo como entrada
  // con resistencia de PULLUP interna del arduino
  pinMode(pinJoystick_SW, INPUT_PULLUP);
  
  Serial.begin(9600);
}

void loop() {
  //Captura de valores analógicos del Joystick
  int valJoystick_X = analogRead(pinJoystick_X);
  int valJoystick_Y = analogRead(pinJoystick_Y);
  
  //Captura de valor digital del Switch del Joystick
  bool valJoystick_SW = !digitalRead(pinJoystick_SW);


  if(valJoystick_Y < 128){
    weight = weight - 1;
  }
  else if(valJoystick_Y < 270) {
    weight = weight - 0.1;
  }
  else if(valJoystick_Y < 480) {
    weight = weight - 0.01;
  }

  if(valJoystick_Y > 896){
    weight = weight + 1;
  }
  else if(valJoystick_Y > 753) {
    weight = weight + 0.1;
  }
  else if(valJoystick_Y > 543) {
    weight = weight + 0.01;
  }
  
  Serial.print("valJoystick_X:");
  Serial.println(valJoystick_X);
  Serial.print("valJoystick_Y:");
  Serial.println(valJoystick_Y);
  Serial.print("valJoystick_SW:");
  Serial.println(valJoystick_SW);
  Serial.print("weight:");
  Serial.println(weight);
  delay(1);
}
